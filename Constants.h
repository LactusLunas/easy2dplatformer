﻿#include <SFML/Graphics.hpp>
#include <iostream>
#include <list>
#include <fstream>
#include <sstream>
#include <ctime>


#define CLASTER_SIZE 4096
#define HEI 105
#define WID 76
#define X_BORDER 550
#define Y_BORDER 300
#define ZA_MONETKU 1
#define ROW_HEIGHT 25
#define SPEED_PER_LEVEL 0.9
#define TEXT_INDENT 10
#define PLATFORM_SPEED 0.2
#define HERO_SPEED 0.4
#define JUMP_IMPULS -0.5
#define PLAYER 2
#define MONETKA 1
#define PLATFORM 3
#define LIFE_BONUS 4
#define RUNNING_BOT 5
#define BULLET 6
#define GAME Singleton::getInstance()

namespace EasyGame
{
	const short FRAMES_OF_HERO_DIE = 3;
	const short FRAMES_OF_HERO_RUN = 8;
	const short FRAMES_OF_HERO_JUMP = NULL;
	const short FRAMES_OF_HERO_STAY = NULL;
	const short FRAMES_OF_HERO_HIT = 2;
	const short FRAMES_OF_BONUS_ROTATE = 10;
	const short MAP_X_SIZE = 31;
	const short MAP_Y_SIZE = 26;
	const short BLOCK_OF_GREED = 9;
	const short BLOCK_OF_LIFE = 10;
	const short LIFE_BONUS_PROB = 33;
        const float GRAVITY = 0.0006;
	const short POOL_SIZE = 10;
	const short BLOCK_SIZE = 50;
	const float TIMER_RUN = 0.005;
	const float PLATFORM_STAY_INDENT_FACTOR = 0.8;
	const float TIMER_TICK = 15;
	const float START_BULLET_SPEED = 0.44012f;
        const short SIZE_OF_FONT = 8;
	const short SIZE_OF_TILES = 12;
	const short SIZE_OF_CURSOR = 5;
	const short SIZE_OF_SPRITE = 114;
	const short TOTAL_CLASTERS = 139;
	
	const sf::Vector2f START_OF_PLATFORM1(150,200);
	const sf::Vector2f SEND_OF_PLATFORM1(550,200);
	
	const sf::Vector2f START_OF_PLATFORM2(630,360);
	const sf::Vector2f SEND_OF_PLATFORM2(1100,360);

	const sf::Vector2f START_OF_MOB(1300,910);
	const sf::Vector2f SEND_OF_MOB(55,910);

	const sf::IntRect PLATFORM_RECT(0,117,195,56);
	const sf::IntRect MOB_RECT(10,190,50,46);
	const sf::IntRect BULLET_RECT(630,12,14,106);
	const sf::String GAME_INSTRUCTION = L"\nНе попадайте под пули.\nСобирайте монетки.\nЗа каждую монетку дается\nодин PTS.\nКогда вы соберете все\nмонетки на поле,\nони автоматически обновятся\nна тех же позициях,\nа игра ускорится.\nЕсли вы столкнетесь\nс лазером,\nто потеряете одну единицу\nздоровья.\nЕсли с мобом,\nбегущим по нижнему ярусу,\n- то все.";
	const sf::String PAUSE_TEXT = L" Подумайте пока над\n своим поведением\n и смыслом жизни.\n Куда вы бежите? Зачем?\n Зачем собираете эти монетки?";
	const sf::String BACKGROUND_CHANGE_INSTRUCTION = L"Введите с клавиатуры путь\nк картинке,\nкою хотите поставить на фон.\nЕжели картинка лежит\nв папке с игрой,\nдостаточно написать ее имя.\nПрямо тут.\nПрямо сейчас.\n\n\n\n\n\n\n";
	const sf::String START_MESSAGE = L"\nВозьмите меня на работу,\nв доме жрать нечего...\n\nШучу, не все так плохо,\nне хотите не берите.\nНо неплохо было бы.";
	const sf::String ERROR_TEXT = L"Сорян, но путь походу неправильный.\n";
	const sf::IntRect PLAYER_RECT(15,452,WID,HEI);
	const std::string record_path = "Records.dat";
	const short LimitCoordY = 1099;
};