﻿#include "Singleton.h"


using namespace std;
using namespace sf;




EasyGame::Button::Button(Vector2f place, String str)
{
	Place = place;
	text = Text(str,*GAME->font,25);
	text.setPosition(place);
}

bool EasyGame::Button::OnTarget()
{
   return (text.getGlobalBounds().contains(Vector2f(Mouse::getPosition().x, Mouse::getPosition().y)));
}

bool EasyGame::Button::Push()
{
   return Mouse::isButtonPressed(Mouse::Left) && OnTarget();
}

void EasyGame::Button::ButtonUpdate()
{
  if(OnTarget()) 
	  text.setColor(Color::Cyan) ;
  else 
	  text.setColor(Color::White);

  if(Push())
	  FuncOfButton();
}

EasyGame::Window::Window(Vector2f TurnOn, Color fontColor)
{
	OnTile.setFillColor(Color::Blue);
	OnTile.setPosition(TurnOn);
	frame.setFillColor(fontColor);
	frame.setPosition(TurnOn);
	frame.setOutlineColor(Color::Cyan);
	frame.setOutlineThickness(4);
	title = Text("", *GAME->font, ROW_HEIGHT);
	title.setColor(Color::White);
	info = Text("", *GAME->font, ROW_HEIGHT);
	info.setColor(Color::White);
        input = Text("", *GAME->font, ROW_HEIGHT);
	input.setColor(Color::Green);
	OnTile.setSize(Vector2f(400,30));
	frame.setSize(Vector2f(400,250));
	insert = false;
	str = str1 = "";
}


EasyGame::Window::Window()
{
	insert = false;
	inActive = false;
}


void EasyGame::Window::Reposition()
{
   list<Button*>::iterator b;

   for(b = buttons.begin(); b != buttons.end(); ++b)
   {
	   (*b)->text.setPosition(Vector2f(frame.getPosition().x + (*b)->Place.x,frame.getPosition().y + (*b)->Place.y));
   }

   title.setPosition(OnTile.getPosition());
   info.setPosition(Vector2f(frame.getPosition().x , frame.getPosition().y + ROW_HEIGHT));
   input.setPosition(Vector2f(frame.getPosition().x + TEXT_INDENT, frame.getPosition().y +  frame.getGlobalBounds().height - 4*ROW_HEIGHT));
}

void EasyGame::Window::WindowUpdate(RenderWindow *window)
{

	   window->draw(frame);
	   window->draw(OnTile);
	   window->draw(title);
	   window->draw(info);
	   window->draw(input);

	   list<Button*>::iterator b;

	    for(b = buttons.begin(); b != buttons.end(); ++b)
		{
		  (*b)->ButtonUpdate();
	          window->draw((*b)->text);
                }

		if(insert)
			Write();
}


float EasyGame::Window::SizeCorrect()
{
	float Correct = 0;
   
	for(short i = 0; i < info.getString().getSize(); ++i)
		if( info.getString()[i] == '\n' )
			Correct += ROW_HEIGHT;

	return Correct;
}

void EasyGame::Window::Resize()
{
	list<Button*>::iterator b;
	
	float X = frame.getSize().x;
	float Y = frame.getSize().y;
	
	float SizeY = SizeCorrect();
	SizeY -= frame.getGlobalBounds().height;

	float buttonsHeight = 0;

	for(b = buttons.begin(); b != buttons.end(); ++b)
	{
	  if(buttonsHeight < ((*b)->Place.y - frame.getGlobalBounds().height))
		  buttonsHeight = (*b)->Place.y -  frame.getGlobalBounds().height;
	}

	SizeY += buttonsHeight;

  if(SizeY > NULL) frame.setSize(Vector2f(X, Y + SizeY));
}

void EasyGame::Window::TextRefresh()
{
  info.setString(str);
  if(str1 != "") info.setString(info.getString() + str1);
}

