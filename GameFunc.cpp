﻿#include "Singleton.h"

using namespace std;
using namespace sf;



unsigned _int8 place;


void Quit()
{
        EasyGame::Clear();
	*GAME->GameState = 0; 
	GAME->level = 1;
	GAME->GameWind.inActive = false;
}

void Restart()
{
  	 if(*GAME->GameOver)
		{
		 EasyGame::Clear();
	         LevelLoader();
	          GAME->GameWind.inActive = false;
		 *GAME->GameOver = false;
		}	 
}

void SubmitNameInScoreTab()
{
        EasyGame::ReFreshScore(GAME->SCR,GAME->GameWind.str1); 
	EasyGame::Clear();
	GAME->GameWind.inActive = false;
	*GAME->GameState = 0;
}

void UnPause()
{
	  GAME->pause = false;
	  GAME->GameWind.inActive = false;
}

void StartOfGame()
{
	  *GAME->GameState = 1;
	  GAME->pause = false;
	  *GAME->GameOver = false;
	  LevelLoader();
}

void ExitFromGame()
{
	 GAME->window->close();
}

void ShowInstruction()
{
  EasyGame::Instruction(GAME->GameWind);
}

void ShowBackGroundWind()
{
  EasyGame::ChangeBackGround(GAME->GameWind);
  GAME->GameWind.insert = true;
}

void ShowScoreTab()
{
    EasyGame::HighScores(GAME->GameWind);
}

void Include()
{
      if(GAME->backG.loadFromFile(GAME->GameWind.str1))
	   {
	        GAME->backGround.setTexture(GAME->backG);
		float SCALE_X = GAME->window->getSize().x/GAME->backGround.getGlobalBounds().width;
		float SCALE_Y = GAME->window->getSize().y/GAME->backGround.getGlobalBounds().height;
	        GAME->backGround.setScale(SCALE_X, SCALE_Y);
	   }
	   else 
	   {
		GAME->GameWind.input.setString(ERROR_TEXT);
	   }
}

void CloseWindow()
{
   GAME->GameWind.inActive = false;
}


unsigned _int8 RecordsHit(short score)
{

	unsigned _int8 position = 1;

	for(int i(0); i < 5;++i)
	{
		if(GAME->R->score[i] > score)  position++;
	}

	return position;
}

EasyGame::Entity* EasyGame::Monetka(Vector2f place)
{
   return new EasyGame::StaticElement(MONETKA, IntRect(0,0,48,45),10,place);	
}

EasyGame::Entity* EasyGame::Live(Vector2f place)
{
   return new EasyGame::StaticElement(LIFE_BONUS, IntRect(642,183,56,50),0,place);	
}


EasyGame::Entity* EasyGame::EscapeLabel()
{
   return new EasyGame::StaticElement(NULL, IntRect(0,257,281,26),0,Vector2f(10,1027));	
}

EasyGame::Entity* EasyGame::NameLabel()
{
   return new EasyGame::StaticElement(NULL, IntRect(298,261,92,23),0,Vector2f(20*BLOCK_SIZE,625));	
}

void EasyGame::GameOver(EasyGame::Window &GameWind, short score)
{
    GameWind =  EasyGame::Window(Vector2f(270,270), Color(0,0,0,205));
    GameWind.title.setString(L"GAME OVER");

	place = RecordsHit(score);

	if(place > 0 && place < 6)
	{
	        GameWind.str = L"Игра окончена!\nВы вошли в сводку рекордов!\n На " + EasyGame::ToString(place) + L" позицию!\nСобрав " + EasyGame::ToString(score) + L" монеток.\nВведите ваше имя:\n\n\n\n\n\n\n\n";
		GameWind.insert = true;

		GameWind.buttons.push_back(new EasyGame::Button(Vector2f(20,250),"Submit"));
		GameWind.buttons.back()->FuncOfButton = &SubmitNameInScoreTab;

		GameWind.buttons.push_back(new EasyGame::Button(Vector2f(310,250),L"Quit"));
		GameWind.buttons.back()->FuncOfButton = &Quit;	
	}
	else
	{
	        GameWind.str = L"Игра окончена!\nВы собрали:\n";
		GameWind.str1 = EasyGame::ToString(score) + L" монетокъ.";

		GameWind.buttons.push_back(new EasyGame::Button(Vector2f(20,200),"Restart"));
		GameWind.buttons.back()->FuncOfButton = &Restart;

		GameWind.buttons.push_back(new EasyGame::Button(Vector2f(310,200),L"Quit"));
		GameWind.buttons.back()->FuncOfButton = &Quit;
	}

     GameWind.On();
}


void EasyGame::Pause(EasyGame::Window &GameWind)
{
    GameWind =  EasyGame::Window(Vector2f(270,270), Color(0,0,0,205));
    GameWind.title.setString(L"               ***PAUSE*** ");
    GameWind.str = EasyGame::PAUSE_TEXT;

    GameWind.buttons.push_back(new EasyGame::Button(Vector2f(20,200),"Continue"));
    GameWind.buttons.back()->FuncOfButton = &UnPause;

    GameWind.buttons.push_back(new EasyGame::Button(Vector2f(310,200),L"Quit"));
    GameWind.buttons.back()->FuncOfButton = &Quit;

    GameWind.On();
}


void EasyGame::GeneralMenu()
{
  GAME->GameMenu = new EasyGame::Window(Vector2f(596,10), Color(0,0,0,155));
  GAME->GameMenu->frame.setSize(Vector2f(300,500));
  GAME->GameMenu->info.setCharacterSize(20);
  GAME->GameMenu->info.setColor(Color::Green);
  GAME->GameMenu->title.setString("NAUTY");
  GAME->GameMenu->OnTile.setSize(Vector2f(300,30));
  GAME->GameMenu->str = EasyGame::START_MESSAGE;

  GAME->GameMenu->buttons.push_back(new EasyGame::Button(Vector2f(20,230),"Game"));
  GAME->GameMenu->buttons.back()->FuncOfButton = &StartOfGame;

  GAME->GameMenu->buttons.push_back(new EasyGame::Button(Vector2f(20,280),"Instruction"));
  GAME->GameMenu->buttons.back()->FuncOfButton = &ShowInstruction;

  GAME->GameMenu->buttons.push_back(new EasyGame::Button(Vector2f(20,330),"Change BackGround"));
  GAME->GameMenu->buttons.back()->FuncOfButton = &ShowBackGroundWind;

  GAME->GameMenu->buttons.push_back(new EasyGame::Button(Vector2f(20,380),"High Score"));
  GAME->GameMenu->buttons.back()->FuncOfButton = &ShowScoreTab;

  GAME->GameMenu->buttons.push_back(new EasyGame::Button(Vector2f(20,430),"Exit"));
  GAME->GameMenu->buttons.back()->FuncOfButton = &ExitFromGame;

  GAME->GameMenu->On();
}

void EasyGame::Instruction(EasyGame::Window &GameWind)
{
	GameWind =  EasyGame::Window(Vector2f(10,10), Color(0,255,255,105));
	GameWind.title.setString(L"Правила игры");
        GameWind.str = EasyGame::GAME_INSTRUCTION;

        GameWind.buttons.push_back(new EasyGame::Button(Vector2f(270,630),"Ok"));
	GameWind.buttons.back()->FuncOfButton = &CloseWindow;

        GameWind.On();
}


void EasyGame::ChangeBackGround(EasyGame::Window &GameWind)
{
	GameWind =  EasyGame::Window(Vector2f(10,10), Color(0,0,0,145));
	GameWind.frame.setSize(Vector2f(570,300));
	GameWind.OnTile.setSize(Vector2f(570,30));
	GameWind.title.setString(L"Настройка фона");
        GameWind.str = EasyGame::BACKGROUND_CHANGE_INSTRUCTION;

        GameWind.buttons.push_back(new EasyGame::Button(Vector2f(470,400),"Include"));
        GameWind.buttons.back()->FuncOfButton = &Include;	

	GameWind.buttons.push_back(new EasyGame::Button(Vector2f(420,400),"Ok"));
	GameWind.buttons.back()->FuncOfButton = &CloseWindow;
    
	GameWind.On();
}


void EasyGame::HighScores(EasyGame::Window &GameWind)
{
	
	GameWind =  EasyGame::Window(Vector2f(10,10), Color(0,55,55,105));
	GameWind.frame.setSize(Vector2f(370,500));
	GameWind.OnTile.setSize(Vector2f(370,30));
	GameWind.title.setString(L"Лучшие результаты");
        GameWind.str = L"\nЗдесь могла бы быть\nваша реклама";

	for(int i = 0; i < 5;++i)
	{
	   GameWind.str += "\n\n" + EasyGame::ToString(i + 1) + ". " + GAME->R->name[i] + L"  -  " + EasyGame::ToString(GAME->R->score[i]);
	}


	GameWind.info.setString(GameWind.info.getString() + GameWind.str);

        GameWind.buttons.push_back(new EasyGame::Button(Vector2f(320,450),"Ok"));
	GameWind.buttons.back()->FuncOfButton = &CloseWindow;
        GameWind.On();
}

void EasyGame::MenuOfGame(RenderWindow *window)
{
  GAME->GameMenu->WindowUpdate(window);
}

void EasyGame::ReFreshScore(short score, String str)
{
   if(place != 5)
   {
     for(int i(0); i != 5 - place;++i)
	{
	   GAME->R->name[4 - i] =  GAME->R->name[3 - i];
	   GAME->R->score[4 - i] = GAME->R->score[3 - i];
	}
   }

	GAME->R->name[place - 1] = string(str);
	GAME->R->score[place - 1] = score;

	ofstream out(record_path, ios::binary);
	out.write(reinterpret_cast<char*>(GAME->R),sizeof(*GAME->R));
	out.close();
}

void EasyGame::LevelLoader()
{ 	
	GAME->pool.push_back(EscapeLabel());
	GAME->pool.back()->s.setScale(1.2,1.75); // Мне лень так много чисел выносить отдельно, тем паче эти встречаются 1 раз
	GAME->pool.push_back(NameLabel());
	GAME->pool.back()->s.setScale(2.5,1.8);
	BonusPlacer();
	GAME->pool.push_back(new DinamicElement(PLATFORM, EasyGame::PLATFORM_RECT,NULL,SEND_OF_PLATFORM1, START_OF_PLATFORM1));
	GAME->pool.push_back(new DinamicElement(PLATFORM, EasyGame::PLATFORM_RECT,NULL,SEND_OF_PLATFORM2, START_OF_PLATFORM2));
	GAME->pool.push_back(new DinamicElement(RUNNING_BOT, EasyGame::MOB_RECT,11,START_OF_MOB, SEND_OF_MOB));
	GAME->pool.back()->s.setScale(3.0,3.0);
	GAME->pool.push_back(new Player(*GAME->PlayerTexture));
}


void EasyGame::ElementsUpdate()
{
	for(GAME->it = GAME->pool.begin(); GAME->it != GAME->pool.end();)
	{
	   (*GAME->it)->update();
	   if(!(*GAME->it)->Life) GAME->it = GAME->pool.erase(GAME->it);
	   else GAME->it++;
	}
}

void EasyGame::Game(Sprite &forMap)
{
	    EasyGame::MapDraw(forMap);
	    EasyGame::SwitchGameState();
	    EasyGame::ElementsUpdate();
	    EasyGame::Interface();
}


////////////////////////////////////////
void EasyGame::SwitchGameState()
{
       if(*GAME->GameOver == false)
		{
		   if(Keyboard::isKeyPressed(Keyboard::P))
	               {
	                   GAME->pause = true;
		           EasyGame::Pause(GAME->GameWind);
	               }


	           if(GAME->pause) 
		       {
			    *GAME->speed = 0.0000001;
		       } // теоретически недосягаемая скорость
	           else 
		       {
			       if(*GAME->speed < 0.000005) *GAME->speed = 1/pow(SPEED_PER_LEVEL, GAME->level); 
		       }

		      EasyGame::LevelUpdate();
		}
		else
		{
		       GAME->level = 1; 
		       *GAME->speed = 1/pow(SPEED_PER_LEVEL, GAME->level);	
		}	 
}
/////////////////////////////////////
void EasyGame::LevelUpdate()
{
  if(Refresh()) 
	{
            BonusPlacer();
            GAME->level++;
            *GAME->speed = 1/pow(SPEED_PER_LEVEL,GAME->level);
	}
  if(TimerTick()) 
	     Shot();
};

void EasyGame::WindowUpdate()
{
	if(GAME->GameWind.inActive) 
		GAME->GameWind.WindowUpdate(GAME->window);
}

void EasyGame::Window::Write()
{
	   if (EasyGame::cooldown(GAME->event->type == sf::Event::TextEntered))

             {
       
        if (GAME->event->text.unicode < 128)

                {
		    
                   if(GAME->event->text.unicode != 8) 
	
                    	{
 
			    str1 += static_cast<char>(GAME->event->text.unicode);

                            input.setString(str1);

              		}

               	    else

			{

			  if(str1 != "")

			  {

			    str1.erase(str1.getSize()-1, 1);

			    input.setString(str1);

			  }

			}

                 }		
	}
}

void EasyGame::DrawBackGround()
{ 
	GAME->window->draw(GAME->backGround);
};