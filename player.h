﻿
#include "Func.h"



namespace EasyGame
{

  struct Player:Entity
  {
	int Score;
	short HP;
	short Bonus;
	float dx,dy;
	bool OnGround;
	bool Hit;
	short live;
	Player(sf::Texture &play);
	void Say(std::string Say);
	void Rule();
	void Gravity();
	void Colliders(short dir);
	void CollWithBonus(short dir);
	void AnimSwitch();
	void RePlacing();
	void ScoreChange();
	void ScoreDraw();
	void TakeAHit();
	void Death();
	
	void update()
	{
		 CameraMove(Position);
		 Animation();
		 Render();
		 RePlacing();
		 TakeAHit();

		 if(live >= 0)
			 AnimSwitch();
		 else
			 Death();
		 
		 if(live > 0) 
			 Rule();
		 else
			 dx = 0;

		 if(Position.y < 0 || Position.y > 1200) 
			 Position.y = 300;
	}
  };
}