﻿#include "Singleton.h"

using namespace std;
using namespace sf;
using namespace EasyGame;

Singleton* Singleton::p_instance = 0;

int main()
{	
	Clock clock;
	srand(time(NULL));
	Uint8 GameState = 0;

	char *c = new char[TOTAL_CLASTERS*CLASTER_SIZE];
	Event event;
	bool GameOver = false;
	RectangleShape block = RectangleShape(Vector2f(50,50));
	block.setFillColor(Color::Blue);
	Texture texture;
	Texture player;
	
	Texture cursorTexture;
	

	int CURRENTBYTE = 0;	 

	ifstream in("Resourse.dat", ios::binary);

	char *b = c;

	for(int i(0); i < TOTAL_CLASTERS*CLASTER_SIZE; ++i)
	{
	   in.read(b,1);
	   c[i] = *b;
	}

	 in.close();


	  CURRENTBYTE += SIZE_OF_FONT* CLASTER_SIZE;
	  texture.loadFromMemory(c + CURRENTBYTE, SIZE_OF_TILES* CLASTER_SIZE);
	  CURRENTBYTE += SIZE_OF_TILES* CLASTER_SIZE;
	  cursorTexture.loadFromMemory(c + CURRENTBYTE, SIZE_OF_CURSOR* CLASTER_SIZE);
	  CURRENTBYTE += SIZE_OF_CURSOR* CLASTER_SIZE;
	  player.loadFromMemory(c + CURRENTBYTE, SIZE_OF_SPRITE * CLASTER_SIZE);


	  Text *text = new Text("",*GAME->font,25);
	  text->setColor(Color::Cyan);
	  text->setPosition(Vector2f(10,10));
	  text->setStyle(Text::Bold);

	  Text *text1 = new Text("",*GAME->font,25);
	  text1->setColor(Color::Magenta);
	  text1->setPosition(Vector2f(10,30));
	  text1->setStyle(Text::Bold);

	  Sprite cursor(cursorTexture);
	  Sprite forMap(texture);


	

	RenderWindow window(VideoMode(900, 800), L"Бегущий чоловiче");
	window.setMouseCursorVisible(false);

	GAME->event = &event;
	GAME->ForHP = text1;
	GAME->ForScore = text;
	GAME->MapTexture = &texture;
	GAME->PlayerTexture = &player;
	GAME->window = &window;

        GeneralMenu();

	while (window.isOpen())
	{
		*GAME->time = clock.getElapsedTime().asMicroseconds(); 
                clock.restart(); 
                *GAME->time = *GAME->time/(800/ *GAME->speed); 

		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}
 	

		cursor.setPosition(Vector2f(Mouse::getPosition().x, Mouse::getPosition().y));

	 	window.clear(Color::Black);
		DrawBackGround();

		if(*GAME->GameState == 0)
		{
		  MenuOfGame(GAME->window);
		}
		if(*GAME->GameState == 1)
		{
		  Game(forMap);
		}

		WindowUpdate();
	
		window.draw(cursor);
		window.display();
	}
 
	delete []c;
	delete GAME;
	return 0;
}