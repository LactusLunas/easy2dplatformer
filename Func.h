﻿#include "GameWindow.h"


namespace EasyGame
{
   // Рисовательство карты
   void MapDraw(sf::Sprite &mapSprite);
   // Загрузка Карты
   void MapLoader();
   // Движение камеры
   void CameraMove(sf::Vector2f Coords);
  // Распределение бонусов (монеток и жизней)
   void BonusPlacer();
   // Возвращает 1, если кол-во монеток = 0
   bool Refresh();
   // Возвращает 1 по истечению времени паузы (для пуль)
   bool TimerTick();
   // Добавляет в лист объектов пулю 
   void Shot();
   // Возвращает Х позицию игрока
   float PlayerCoordX();
   // Возвращает кол-во собранных монеток
   short PlayerScore();
   // Возвращает строковое выражение входящего аргумента 
   std::string ToString(short var);
   // Образует игровое меню
   void GeneralMenu();
   // Отрисовывает игровое меню
   void MenuOfGame(sf::RenderWindow *window);
   // Отрисовывает фоновое изображение
   void DrawBackGround();
   // Алгоритм завершения игры
   void GameOver(EasyGame::Window &GameWind, short score);
   // Алгоритм пазуы, создание окна паузы
   void Pause(EasyGame::Window &GameWind);
   // Обновление уровня (игры)
   void LevelUpdate();
   // Переключение игрового состояния (пауза/game over)
   void SwitchGameState();
   // Обновление элементов на карте
   void ElementsUpdate();
   // Очистка поля от старых элементов 
   void ClearArea();
   // Игра
   void Game(Sprite &forMap);
   // Отображение информации о состоянии игры
   void Interface();
   // Очистка поля игры (удаление всех объектов с карты)
   void Clear();
   // Старт игры
   void LevelLoader();
   // образует окно с инструкцией
   void Instruction(EasyGame::Window &GameWind);
   // образует окно с рекордами
   void HighScores(EasyGame::Window &GameWind);
   // Образует окно настройки фона
   void ChangeBackGround(EasyGame::Window &GameWind);
   // Обновление игрового окна
   void WindowUpdate();
   // обновление числа, отражающего кол-во собранных монеток
   void ReFreshScore(short score, String str);
   // CoolDown of Event
   bool cooldown(bool trigger);
   // возвращает объект (монетку) в указанном месте
   Entity* Monetka(sf::Vector2f place);
   // Возвращает бонус к здоровью в указанном месте
   Entity* Live(sf::Vector2f place);
   // Создает надпись на платформе
   Entity* EscapeLabel();
   Entity* NameLabel();
   // Возвращает объект-платформу в указанном месте
   Entity* platform(sf::Vector2f place);
}

