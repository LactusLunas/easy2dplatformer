﻿#include <iostream>
#include <math.h>
#include "Singleton.h"
#include "Map.h"

using namespace std;
using namespace sf;
using namespace EasyGame;


Singleton::Singleton()
:GameOver(false), OffSetX(0), OffSetY(0), currentTimer(0), pause(false), _cooldown(0), level(1)
{
	
      R = new EasyGame::Records();

      ifstream in(record_path, ios::binary);
      in.read(reinterpret_cast<char*>(R),sizeof(*R));
      in.close();

       font = new Font();
       font->loadFromFile("open-sans.ttf");

       GameWind = EasyGame::Window();
       GameMenu = new EasyGame::Window();

       time = new float(0);
       speed = new float(1.0);
       GameOver = new bool(0);
       GameState = new Uint8(0);
}

Vector2i startTile()
{
	return Vector2i(GAME->OffSetX/BLOCK_SIZE, GAME->OffSetY/BLOCK_SIZE);
}


bool EasyGame::cooldown(bool trigger)
{
	Singleton::getInstance()->_cooldown += TIMER_RUN**Singleton::getInstance()->time;

	if(Singleton::getInstance()->_cooldown > 1 && trigger)
	{
	  Singleton::getInstance()->_cooldown = 0;
	  return true;
	}
	else return false;
}


void EasyGame::Entity::Render()
{
	s.setPosition(Vector2f(Position.x - Singleton::getInstance()->OffSetX, Position.y - Singleton::getInstance()->OffSetY));
	Singleton::getInstance()->window->draw(s);
}


void EasyGame::CameraMove(Vector2f Coords)
{
   if(Coords.x > Singleton::getInstance()->window->getSize().x/2)
	   Singleton::getInstance()->OffSetX = Coords.x - Singleton::getInstance()->window->getSize().x/2;

   if(Coords.y > Singleton::getInstance()->window->getSize().y/2)
	   Singleton::getInstance()->OffSetY = Coords.y - Singleton::getInstance()->window->getSize().y/2;

   if(Singleton::getInstance()->OffSetX > X_BORDER) 
	   Singleton::getInstance()->OffSetX = X_BORDER;

   if(Singleton::getInstance()->OffSetY > Y_BORDER)
	   Singleton::getInstance()->OffSetY = Y_BORDER;
}


Entity::Entity()
{
	vector = 0;
	CurrentFrame = 0;
}

void Entity::Death()
{
	Life = false;
}

void EasyGame::Entity::Animation()
{
    if(upFrame !=0)
	{
           CurrentFrame += TIMER_RUN**Singleton::getInstance()->time;
	
	   if(CurrentFrame > upFrame)
		{CurrentFrame -= upFrame;}

	    s.setTextureRect(IntRect(textureRect.left + short(CurrentFrame + vector)*textureRect.width, textureRect.top, textureRect.width - 2* textureRect.width*vector,textureRect.height));
	}
}


EasyGame::StaticElement::StaticElement(_int8 identity,IntRect imageRect, short up, Vector2f place)
{
  Life = true;
  kind = identity;
  textureRect = imageRect;
  Position = place;
  upFrame = up;
  s.setTexture(*GAME->MapTexture);
  s.setTextureRect(textureRect);
}


EasyGame::DinamicElement::DinamicElement(_int8 identity,IntRect imageRect, short up, Vector2f place, Vector2f SendTo)
{
  dx = -0.2;
  dy = NULL;
  kind = identity;
  Life = true;
  textureRect = imageRect;
  Position = place;
  SendPlace = SendTo;
  upFrame = up;
  s.setTexture(*GAME->MapTexture);
  s.setTextureRect(textureRect);
  StartPlace = place;
}


void EasyGame::DinamicElement::Move()
{
	if(short(Position.x) <= SendPlace.x) 
	{
	  vector= 0; dx = PLATFORM_SPEED; 
	}
	if(short(Position.x) >= StartPlace.x) 
	{
	  vector = 1; dx = -PLATFORM_SPEED;
	}
	

	Position.x += dx**GAME->time;
	Position.y += dy**GAME->time;
	s.setPosition(Vector2f(Position.x - GAME->OffSetX, Position.y - GAME->OffSetY));
	GAME->window->draw(s);
}

void EasyGame::MapDraw(Sprite &mapSprite)
{
	Vector2i PointFrom = startTile();

        for(int i = PointFrom.x; i < PointFrom.x + GAME->window->getSize().x/BLOCK_SIZE + 1;++i)
	  for(int k = PointFrom.y; k < PointFrom.y + GAME->window->getSize().y/BLOCK_SIZE + 1;++k)
	  {
		 if(map[k][i] >= 2 && map[k][i] <= 8)
		{
		  mapSprite.setTextureRect(IntRect(0 + BLOCK_SIZE*(map[k][i]-2),59,BLOCK_SIZE,BLOCK_SIZE));
		  mapSprite.setPosition(BLOCK_SIZE*i - GAME->OffSetX, BLOCK_SIZE*k - GAME->OffSetY);
		  GAME->window->draw(mapSprite);
		}
	  }
}


EasyGame::Player::Player(Texture &play):Hit(false), Score(NULL),dx(NULL),dy(NULL)
{
   name = "nauty";
   Hit = false;
   kind = PLAYER;
   Life = true;
   live = 3;
   vector = 1;
   OnGround = false;
   Position = Vector2f(350,300);
   textureRect = PLAYER_RECT;
   s.setTexture(play);
   s.setTextureRect(textureRect);
   s.setPosition(Position);
   upFrame = 8;
   RePlacing();
   ScoreChange();
}


void EasyGame::Player::RePlacing()
{
	OnGround = false;
	Position.x += dx**GAME->time;
	Colliders(NULL);
	CollWithBonus(NULL);
	Position.y += dy**GAME->time;
	Colliders(1);
	CollWithBonus(1);
}


void EasyGame::Player::Death()
{
	GAME->SCR = Score;

	if(live < NULL && *GAME->GameOver == false) 
		*GAME->GameOver = true;

	 if(!GAME->GameWind.inActive)
		 EasyGame::GameOver(GAME->GameWind, Score);
}

void EasyGame::Player::Rule()
{
	  if(Keyboard::isKeyPressed(Keyboard::A))
	  {
	   dx = -HERO_SPEED; vector = 1;
	  }
          else dx = NULL;

          if(Keyboard::isKeyPressed(Keyboard::D))
	  {
	   dx = HERO_SPEED;
           vector = NULL;
	  }  
	  if(Keyboard::isKeyPressed(Keyboard::W) && OnGround)
	  {
	   OnGround = false;
	   dy = JUMP_IMPULS;
	  }  
}

void EasyGame::Player::Gravity()
{
  if(!OnGround)
	  dy += GRAVITY**GAME->time;
}

void EasyGame::Player::Colliders(short dir)
{
  			for (int i = Position.y / BLOCK_SIZE; i < (Position.y + HEI) / BLOCK_SIZE; i++)
			for (int j = Position.x / BLOCK_SIZE; j<(Position.x + WID) / BLOCK_SIZE; j++)
			{
				if (map[i][j] >= 1 && map[i][j] <=5)
				{
					if (dy > 0 && dir == 1)
					{
						Position.y = i * BLOCK_SIZE - textureRect.height ; OnGround = true; dy = NULL;
					}
					if (dy<0 && dir == 1)
					{
						dy = 0; Position.y = i * BLOCK_SIZE + BLOCK_SIZE;
					}
					if (dx>0 && dir == 0)
					{
						Position.x = j * BLOCK_SIZE - textureRect.width;
						
					}
					if (dx < 0 && dir == 0)
					{
						Position.x = j * BLOCK_SIZE + BLOCK_SIZE;
					}
				}

			}

			if(dir == NULL)Gravity();	
}

void EasyGame::Player::AnimSwitch()
{
	/* 
	   У меня спрайт очень не удобный. Поэтому пришлось прописывать все состояния.
	   А так я бы мог на каждое изменение состояния персонажа 
	   просто изменять параметр upFrame
	*/

	if(OnGround)
         {
	     if(dx == 0 && textureRect.left != 95 ) 
              {
                textureRect = IntRect(95 + WID*vector,753,WID - 2*WID*vector,HEI);
	        s.setTextureRect(textureRect);
	        upFrame = FRAMES_OF_HERO_STAY;
              }

          if(dx != 0 && textureRect.left !=15)
              {
               textureRect = IntRect(15,452,WID,HEI);
	       s.setTextureRect(textureRect);
     	       upFrame = FRAMES_OF_HERO_RUN;
              }
	}
	else
	{
	  textureRect = IntRect(227 + WID*vector,663,WID -2*WID*vector,HEI);
	  s.setTextureRect(textureRect);
	  upFrame = FRAMES_OF_HERO_JUMP;
	}
	
	if(live == NULL)
	{
		Hit = false;
		s.setColor(Color::White);
	        textureRect = IntRect(190 + WID*vector,776,WID -2*WID*vector,HEI);
	        s.setTextureRect(textureRect);
	        upFrame = FRAMES_OF_HERO_DIE;

	        if(short(CurrentFrame) == 2) 
                      {  
                         live = -1;
                      }
	}

	if(live == -1)
	{
	   textureRect = IntRect(348 + WID*vector,760,WID -2*WID*vector,HEI);
	   s.setTextureRect(textureRect);
	   upFrame = NULL;
	}
	
	if(Hit)
	{
	   textureRect = IntRect(18 + WID*vector,752,WID -2*WID*vector,HEI);
	   s.setTextureRect(textureRect);
	   upFrame = FRAMES_OF_HERO_HIT;
	}
}


void EasyGame::Player::TakeAHit()
{
	 if(Hit)
	 {
	    s.setColor(Color::Red);

	    if(EasyGame::cooldown(true))
	     {
		   Hit = false;
                   s.setColor(Color::White);
	     }
	 }
}


void EasyGame::Player::CollWithBonus(short dir)
{
	list<Entity*>::iterator i;

	for(i = GAME->pool.begin(); i != GAME->pool.end();i++)
	{
	  if((*i)->s.getGlobalBounds().intersects(s.getGlobalBounds()))
	 {
		 switch((*i)->kind)
		 {
		 case MONETKA:
			 {
			   if(dir == NULL)
			    {

				Score += MONETKA;
				ScoreChange();
				(*i)->Death();
			    
			   } 

			   break;
			 }

		 case PLATFORM:
			 {
			  
		         if(dy > 0 && dir == 1)
		         {
			  
			   Position.x += (*i)->dx**GAME->time;
			   Position.y = (*i)->Position.y - HEI * PLATFORM_STAY_INDENT_FACTOR;
			   dy = NULL; 
			   OnGround = true; 

		          }

		         if(Position.y > (*i)->Position.y + PLATFORM_RECT.height - 1)
		          { 
				 Position.y += 4; dy = NULL;
		          }

			  break;
			 }

		 case RUNNING_BOT:
		  {
		      if(live != -1)
		      {

		        live = NULL;
		       ScoreChange();
	              }

		    break;
		  }

		 case BULLET:
		  {
			   Hit = true;
		           (*i)->Life = false;

		     if(dir == NULL && live > NULL) 
		     {

		      --live;
		      if(dy < NULL)
				  dy = NULL;

		      ScoreChange();

		     }

			 break;
		  }

		 case LIFE_BONUS:
		  {
		      if(dir == NULL)
			  {
				  live++; (*i)->Life = false; 
				  ScoreChange(); 
			  }

			  break;
		  }
		 }
	  }
	}
}

void EasyGame::BonusPlacer()
{
  for(int i(0); i < MAP_X_SIZE;++i)
	  for(int k(0); k < MAP_Y_SIZE; ++k)
	  {
	    if (map[k][i] == BLOCK_OF_GREED)
		{
			GAME->pool.push_back(Monetka(Vector2f(i*BLOCK_SIZE, k*BLOCK_SIZE)));
		}
		if(map[k][i] == BLOCK_OF_LIFE)
		{
			if((rand()%100 + 1) < LIFE_BONUS_PROB)
		       GAME->pool.push_back(Live(Vector2f(i*BLOCK_SIZE, k*BLOCK_SIZE)));
		}
	  }
}

bool EasyGame::Refresh()
{
   short monetki = NULL;

   for(GAME->it = GAME->pool.begin(); GAME->it != GAME->pool.end(); GAME->it++)
   {
     if((*GAME->it)->kind == MONETKA) monetki++;
   }
	return (monetki == NULL);
}

bool EasyGame::TimerTick()
{
	GAME->currentTimer += TIMER_RUN**GAME->time;

	if(GAME->currentTimer > TIMER_TICK)
	{
	   GAME->currentTimer = NULL;
	   return true;
	}
	else 
		return false;
}

float EasyGame::PlayerCoordX()
{
   for(GAME->it = GAME->pool.begin(); GAME->it != GAME->pool.end(); GAME->it++)
   {
     if((*GAME->it)->kind == PLAYER) 
         return (*GAME->it)->Position.x + 20;
   }

   return -1.0;
}


string EasyGame::ToString(short var)
{
                ostringstream playerScoreString;
		playerScoreString << var;
		return playerScoreString.str();
}

void EasyGame::Player::ScoreChange()
{
   GAME->ForScore->setString("SCORE:" + EasyGame::ToString(Score));
   GAME->ForHP->setString("LIVES: " + EasyGame::ToString(live));
}


void EasyGame::Interface()
{
   GAME->window->draw(*GAME->ForHP);
   GAME->window->draw(*GAME->ForScore);
}

void EasyGame::Shot()
{
	GAME->pool.push_back(new DinamicElement(BULLET, EasyGame::BULLET_RECT,0,Vector2f(EasyGame::PlayerCoordX(),-100), Vector2f(EasyGame::PlayerCoordX(),1100)));
	GAME->pool.back()->dy = START_BULLET_SPEED;
}


void EasyGame::Clear()
{
	for(GAME->it = GAME->pool.begin(); GAME->it != GAME->pool.end();)
		{
		 if((*GAME->it)->Life) GAME->it = GAME->pool.erase(GAME->it);
		 else GAME->it++;
		}
}
