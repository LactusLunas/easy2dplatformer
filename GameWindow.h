﻿#include <SFML/Graphics.hpp>
#include <list>
#include <iostream>
#include "Entity.h"



namespace EasyGame
{
	struct Button
	{
	  sf::Text text;
	  sf::Vector2f Place;
	  Button(sf::Vector2f place, sf::String str);
	  bool Push();
	  bool OnTarget();
	  void ButtonUpdate();
	  void (*FuncOfButton)();
	};

	struct Window
    {
		float DrawPoint;
		bool inActive;
		bool insert;
		sf::RectangleShape frame;
		sf::RectangleShape OnTile;
		sf::Text title;
		sf::Text info;
		sf::Text input;
		sf::String str,str1;
		std::list<Button*> buttons;
		std::list<Button*>::iterator it;
		Window(sf::Vector2f TurnOn, sf::Color fontColor);
		Window();
		void TextRefresh();
		void Reposition();
		void Resize();
		void WindowUpdate(sf::RenderWindow *window);
		float SizeCorrect();
		void Write();
		void On()
		{
		   TextRefresh();
		   Resize();
		   Reposition();
		   inActive = true;
		}
    };
}
