﻿#include "StaticElements.h"



namespace EasyGame
{
  struct DinamicElement:Entity
  {
		DinamicElement(_int8 identity, sf::IntRect imageRect, short up, sf::Vector2f place, sf::Vector2f SendTo);
		sf::Vector2f StartPlace;
		sf::Vector2f SendPlace;
		

		void Move();

		void Death()
		{
			if(Position.y > LimitCoordY)
			{
				Life = false;
			} 
		};

		void update()
		{
		  Animation();
		  Move();
		  Death();
		}
  };
}