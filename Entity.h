﻿#include "Constants.h"


#ifndef ENTITY_H_

#define ENTITY_H_


namespace EasyGame
{

   struct Records
    {
	 std::string name[5];
	 short score[5];
    };


   struct Entity
   {
         sf::Sprite s;
	 float dx,dy;
	 _int8 kind;
         std::string name;
	 short vector;
	 short upFrame;
	 float CurrentFrame;
	 bool Life;
	 sf::IntRect textureRect;
	 sf::Vector2f Position;
	 Entity();
	 Entity(sf::IntRect imageRect, short down, short up, sf::Vector2f place);
	 void Animation();
	 void Render();
	 virtual void update() = 0;
	 virtual void Death();
   };
}


#endif