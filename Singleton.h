﻿#include "DinamicElements.h"
 
using namespace std;
using namespace sf;
using namespace EasyGame;


class Singleton
{
  private:
    	static Singleton *p_instance;
    	Singleton();
    	Singleton( const Singleton& );  
    	Singleton& operator=( Singleton& );
  public:
	  bool *GameOver;
	  float *time;
	  Uint8 *GameState;
	  float *speed;
	  RenderWindow *window;
	  Texture *MapTexture;
	  Texture *PlayerTexture;
	  Text *ForHP; 
	  Text *ForScore;
	  Event *event;
	  Font *font;
	  list<EasyGame::Entity*> pool;
          list<EasyGame::Entity*>::iterator it;
	  Texture backG;
      	  Sprite backGround;
	  Records *R;

	  EasyGame::Window GameWind;
	  EasyGame::Window *GameMenu;
          short level;
          bool pause;
          float _cooldown ;
          short SCR;

          float OffSetX;
	  float OffSetY ;
          float currentTimer;

    static Singleton * getInstance() 
	{

           if(!p_instance)           
            p_instance = new Singleton();

           return p_instance;

        }
};